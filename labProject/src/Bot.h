//
// Created by Administrator on 1/25/2022.
//

#ifndef CMAKEDEMO_BOT_H
#define CMAKEDEMO_BOT_H
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include "Vector.h"
class Bot {
    vect3 position;
    vect3 size;
    float speed;
public:
    Bot(vect3 _size,float _speed);
    void DrawBot();

    void MoveX(vect2 xMovement);
    void MoveZ(vect2 zMovement);

};


#endif //CMAKEDEMO_BOT_H
