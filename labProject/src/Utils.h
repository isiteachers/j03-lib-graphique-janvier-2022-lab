//
// Created by jlidou on 2022-01-19.
//

#ifndef CMAKEDEMO_UTILS_H
#define CMAKEDEMO_UTILS_H
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
void drawAxis(float size);
void drawCube(float sizeX,float sizeY,float sizeZ);
SDL_Surface * flipSurface(SDL_Surface * surface);
GLuint loadTexture(std::string path);
#endif //CMAKEDEMO_UTILS_H
