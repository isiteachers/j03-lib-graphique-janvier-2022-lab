//
// Created by Administrator on 1/25/2022.
//

#ifndef J03_LIB_GRAPHIQUE_JANVIER_2022_LAB_BOX_H
#define J03_LIB_GRAPHIQUE_JANVIER_2022_LAB_BOX_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Vector.h"
#include "Camera.h"
#include "Utils.h"

class Box {
private:
    vect3 position;
    vect3 size;
    GLuint idImageBox;

    void drawBox(float sizeX, float sizeY, float sizeZ, GLuint &idImage);
public:
    Box(const vect3 &position, const vect3 &size);
    const vect3 &getPosition() const;
    const vect3 &getSize() const;

    void drawBox();
    void deleteTexture();
};


#endif //J03_LIB_GRAPHIQUE_JANVIER_2022_LAB_BOX_H
