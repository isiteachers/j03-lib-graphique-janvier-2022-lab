#include "InputManager.h"

void InputManager::checkInputs(const Uint8* state, Player *player)
{
    vect3 movement = {0,0,0};
    float rotation = 0;

    if (state[MOVE_FORWARD_KEY])
    {
        movement.x++;
    }
    if (state[MOVE_BACKWARD_KEY])
    {
        movement.x--;
    }
    if (state[ROTATE_LEFT_KEY])
    {
        rotation++;
    }
    if (state[ROTATE_RIGHT_KEY])
    {
        rotation--;
    }

    player->move(movement, rotation);
}
