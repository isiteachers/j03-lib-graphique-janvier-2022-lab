//
// Created by Administrator on 1/25/2022.
//

#include "Box.h"

Box::Box(const vect3 &position, const vect3 &size) : position(position), size(size){
    idImageBox = loadTexture("assets/box.jpg");
}

void Box::drawBox() {
    glPushMatrix();
    glTranslatef(position.x, position.y, position.z);
    drawBox(size.x, size.y, size.z, idImageBox);
    glPopMatrix();
}

void Box::drawBox(float sizeX,float sizeY,float sizeZ, GLuint &idImage) {
    glPushMatrix();
    glScalef(sizeX, sizeY, sizeZ);
    glBindTexture(GL_TEXTURE_2D, idImageBox);
    glBegin(GL_QUADS);
    //front (x axis)
    glColor3f(1, 1, 1);
    glTexCoord2f(2,2); glVertex3f(1, 1, 1);
    glTexCoord2f(0,2); glVertex3f(1, -1, 1);
    glTexCoord2f(0,0); glVertex3f(1, -1, -1);
    glTexCoord2f(2,0); glVertex3f(1, 1, -1);
    //left
    glTexCoord2f(2,2); glVertex3f(1, 1, -1);
    glTexCoord2f(0,2); glVertex3f(1, -1, -1);
    glTexCoord2f(0,0); glVertex3f(-1, -1, -1);
    glTexCoord2f(2,0); glVertex3f(-1, 1, -1);
    //right
    glTexCoord2f(2,2); glVertex3f(-1, 1, 1);
    glTexCoord2f(0,2); glVertex3f(-1, -1, 1);
    glTexCoord2f(0,0); glVertex3f(1, -1, 1);
    glTexCoord2f(2,0); glVertex3f(1, 1, 1);
    //back
    glTexCoord2f(2,2); glVertex3f(-1, 1, -1);
    glTexCoord2f(0,2); glVertex3f(-1, -1, -1);
    glTexCoord2f(0,0); glVertex3f(-1, -1, 1);
    glTexCoord2f(2,0); glVertex3f(-1, 1, 1);
    //top
    glTexCoord2f(2,2); glVertex3f(1, 1, -1);
    glTexCoord2f(0,2); glVertex3f(-1, 1, -1);
    glTexCoord2f(0,0); glVertex3f(-1, 1, 1);
    glTexCoord2f(2,0); glVertex3f(1, 1, 1);
    //bottom
    glTexCoord2f(2,2); glVertex3f(1, -1, -1);
    glTexCoord2f(0,2); glVertex3f(-1, -1, -1);
    glTexCoord2f(0,0); glVertex3f(-1, -1, 1);
    glTexCoord2f(2,0); glVertex3f(1, -1, 1);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glPopMatrix();
}

const vect3 &Box::getPosition() const {
    return position;
}

const vect3 &Box::getSize() const {
    return size;
}

void Box::deleteTexture() {
    glDeleteTextures(1,&idImageBox);
}
