#include "Player.h"

vect3 Player::getPosition()
{
    return position;
}

float Player::getRotation()
{
    return rotation;
}

void Player::draw()
{
    glPushMatrix();
    glTranslatef(position.x, position.y, position.z);
    glRotatef(rotation, 0, 1, 0);
    drawCube(1, 1, 1);
    glPopMatrix();
}

Player::Player(vect3 position, float velocity, float rotationSpeed) : position(position)
{
    this->velocity = velocity;
    this->rotationSpeed = rotationSpeed;
}

void Player::move(vect3 movement, float rotation)
{
    vect3 forwardVect = forward();
    position.x += movement.x * forwardVect.x;
    position.z += movement.x * forwardVect.z;
    this->rotation += rotation * rotationSpeed;
}

vect3 Player::forward()
{
    float x = std::sin(M_PI * (90 + rotation)/ 180);
    float z = std::cos(M_PI * (90 + rotation)/ 180);
    return {x, 0, z};
}
