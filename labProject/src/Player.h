#ifndef CMAKEDEMO_PLAYER_H
#define CMAKEDEMO_PLAYER_H
#include <GL/gl.h>
#include <GL/glu.h>
#include "Utils.h"
#include "Vector.h"

class Player {
private:
    vect3 position;
    float velocity;
    float rotationSpeed;
    float rotation = 0;

    vect3 forward();

public:
    Player(vect3 position, float velocity, float rotationSpeed);

    vect3 getPosition();
    float getRotation();

    void draw();
    void move(vect3 movement, float rotation);


};


#endif
