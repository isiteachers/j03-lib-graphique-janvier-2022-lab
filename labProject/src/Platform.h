//
// Created by Charles on 1/25/2022.
//

#ifndef CMAKEDEMO_PLATFORM_H
#define CMAKEDEMO_PLATFORM_H

#include <GL/gl.h>
#include <GL/glu.h>
#include "Vector.h"
#include"Utils.h"

class Platform {
vect3 position;
vect3 size;

GLuint idtexture;
GLuint idtexture2;

public:
    Platform();
    void draw();
};


#endif //CMAKEDEMO_PLATFORM_H
