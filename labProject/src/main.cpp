#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Vector.h"
#include "Camera.h"
#include "Utils.h"
#include "Player.h"
#include "InputManager.h"
#include "Platform.h"
#include <vector>
#include "Box.h"
#include <stdlib.h>
#include <time.h>
#include "Bot.h"

int main(int argc, char **args) {
    SDL_Window *win;
    int width = 1024, height = 768;
    bool isRunning = true;
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    win = SDL_CreateWindow("OpenGl Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
                           SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GLContext context = SDL_GL_CreateContext(win);
    SDL_GL_SetSwapInterval(1);

    Bot* bot =new Bot({2,2,2},0.1f);
    Bot* bot1 =new Bot({1,1,1},0.1f);
    //try load texture

    Camera *camera = new Camera();
    vect3 camPosition = {-10, 5, -10};
    camera->setPosition(camPosition);

    std::vector<Box*> boxes;

    srand(time(0));
    for(float i = 0; i < 10; i++)
    {
        boxes.push_back(new Box({static_cast<float>(rand() % 19 + 1),1,static_cast<float>(rand() % 19 + 1)},{1,1,1}));
    }
    //declaration of quadric

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);

    //explain how to project evrything on the screen

    const Uint8 *state = nullptr;
    SDL_Event event;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, (double) (width / height), .01, 1000);
    glMatrixMode(GL_MODELVIEW);

    //init object
    auto *platform = new Platform();
    Player* player = new Player({0,0,0}, 1, 5);
    //boucle de jeu
    while (isRunning) {
        bot->MoveX({-10,10});
        bot1->MoveZ({-10,10});
//        //clean render
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //update modele
        //with event
        SDL_PollEvent(&event);
        state = SDL_GetKeyboardState(NULL);
        if (event.type == SDL_QUIT) {
            isRunning = false;
        }

        InputManager::checkInputs(state, player);

        // without event

        bot->DrawBot();
        bot1->DrawBot();

        glLoadIdentity();
        camera->cameraFollow(player->getPosition(), player->getRotation());
        camera->look();
//        // look into scren
        platform->draw();
        drawAxis(3);

        player->draw();
        for(Box* box: boxes)
            box->drawBox();
        //update the screen
        glFlush();
        SDL_GL_SwapWindow(win);
//        //break in loop
        SDL_Delay(1);
    }
    delete platform;
    delete camera;
    for(Box* box: boxes)
        box->deleteTexture();
    SDL_DestroyWindow(win);
    IMG_Quit();
    SDL_Quit();
    return 0;
}

