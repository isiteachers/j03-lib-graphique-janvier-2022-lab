#ifndef CMAKEDEMO_INPUTMANAGER_H
#define CMAKEDEMO_INPUTMANAGER_H
#include <SDL2/SDL.h>
#include "Player.h"


class InputManager {
private:
    static const Uint8 MOVE_FORWARD_KEY = SDL_SCANCODE_W;
    static const Uint8 MOVE_BACKWARD_KEY = SDL_SCANCODE_S;
    static const Uint8 ROTATE_LEFT_KEY = SDL_SCANCODE_A;
    static const Uint8 ROTATE_RIGHT_KEY = SDL_SCANCODE_D;

public:
    static void checkInputs(const Uint8* state, Player* player);

};


#endif
