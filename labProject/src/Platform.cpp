//
// Created by Charles on 1/25/2022.
//

#include "Platform.h"

void Platform::draw() {
    glPushMatrix();
    glTranslatef(position.x,position.y - size.y,position.z);
    glScalef(size.x,size.y,size.z);
    glBindTexture(GL_TEXTURE_2D, idtexture);
    glBegin(GL_QUADS);
    glColor3f(1,1,1);

    //TOP
    glTexCoord2i(2,2);glVertex3f(1, 1, -1);
    glTexCoord2i(0,2);glVertex3f(-1, 1, -1);
    glTexCoord2i(0,0);glVertex3f(-1, 1, 1);
    glTexCoord2i(2,0);glVertex3f(1, 1, 1);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, idtexture2);
    glBegin(GL_QUADS);
    //BOTTOM
    glTexCoord2i(0,0);glVertex3f(-1, -1, -1);
    glTexCoord2i(0,2);glVertex3f(-1, -1, 1);
    glTexCoord2i(2,2);glVertex3f(1, -1, 1);
    glTexCoord2i(2,0);glVertex3f(1, -1, -1);

    //SideLeft
    glTexCoord2i(0,0);glVertex3f(-1, -1, 1);
    glTexCoord2i(5,0);glVertex3f(1, -1, 1);
    glTexCoord2i(5,1);glVertex3f(1, 1, 1);
    glTexCoord2i(0,1);glVertex3f(-1, 1, 1);

    //SideBack
    glTexCoord2i(5,0);glVertex3f(-1, -1, 1);
    glTexCoord2i(0,0);glVertex3f(-1, -1, -1);
    glTexCoord2i(0,1);glVertex3f(-1, 1, -1);
    glTexCoord2i(5,1);glVertex3f(-1, 1, 1);

    //SideRight
    glTexCoord2i(0,0);glVertex3f(-1, -1, -1);
    glTexCoord2i(5,0);glVertex3f(1, -1, -1);
    glTexCoord2i(5,1);glVertex3f(1, 1, -1);
    glTexCoord2i(0,1);glVertex3f(-1, 1, -1);

    //SideFace
    glTexCoord2i(0,0);glVertex3f(1, -1, 1);
    glTexCoord2i(5,0);glVertex3f(1, -1, -1);
    glTexCoord2i(5,1);glVertex3f(1, 1, -1);
    glTexCoord2i(0,1);glVertex3f(1, 1, 1);

    glEnd();
    glBindTexture(GL_TEXTURE_2D,0);
    glPopMatrix();
}

Platform::Platform() {
    idtexture = loadTexture("../assets/floor.png");
    idtexture2 = loadTexture("../assets/floor.jpg");
    size = {20,1,20};
    position = {0,0,0};
}
