//
// Created by jlidou on 2022-01-19.
//

#ifndef CMAKEDEMO_VECTOR_H
#define CMAKEDEMO_VECTOR_H
struct vect3 {
    float x,y,z;
    vect3 operator-(vect3 &toRemove) {
        vect3 ret = {(x - toRemove.x), (y - toRemove.y), (z - toRemove.z)};
        return ret;
    }
};
struct vect2{
    float x,y;
};
#endif //CMAKEDEMO_VECTOR_H
