//
// Created by jlidou on 2022-01-19.
//

#ifndef CMAKEDEMO_CAMERA_H
#define CMAKEDEMO_CAMERA_H
#include "Vector.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>
class Camera {
private:
    vect3 position;
    vect3 target;
public:
    Camera();
    Camera(const vect3 &position, const vect3 &target);
    const vect3 &getPosition() const;
    void setPosition(const vect3 &position);
    const vect3 &getTarget() const;
    void setTarget(const vect3 &target);
    void look();
    void cameraFollow(vect3 target, float rotation);

};
#endif //CMAKEDEMO_CAMERA_H
