//
// Created by Administrator on 1/25/2022.
//

#include "Bot.h"

Bot::Bot(vect3 _size,float _speed) {
size = _size;
speed =_speed;
position = {0,0,0};
}

void Bot::DrawBot() {
    glPushMatrix();
    glTranslatef(position.x,position.y,position.z);
    glScalef(size.x, size.y, size.z);
    glBegin(GL_QUADS);
    //front (x axis)
    glColor3f(1, 1, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(1, -1, 1);
    glVertex3f(1, -1, -1);
    glVertex3f(1, 1, -1);
    //left
    glColor3f(0.9, 0.9, 0.9);
    glVertex3f(1, 1, -1);
    glVertex3f(1, -1, -1);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, 1, -1);
    //right
    glColor3f(0.8, 0.8, 0.8);
    glVertex3f(-1, 1, 1);
    glVertex3f(-1, -1, 1);
    glVertex3f(1, -1, 1);
    glVertex3f(1, 1, 1);
    //back
    glColor3f(.7, .7, .7);
    glVertex3f(-1, 1, -1);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, -1, 1);
    glVertex3f(-1, 1, 1);
    //top
    glColor3f(0.6, 0.6, 0.6);
    glVertex3f(1, 1, -1);
    glVertex3f(-1, 1, -1);
    glVertex3f(-1, 1, 1);
    glVertex3f(1, 1, 1);
    //bottom
    glColor3f(0.5, 0.5, 0.5);
    glVertex3f(1, -1, -1);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, -1, 1);
    glVertex3f(1, -1, 1);
    glEnd();
    glPopMatrix();
}

void Bot::MoveX(vect2 xMovement) {

    if (position.x>xMovement.y)
    {
        speed = speed*-1;
    }
    if (position.x<xMovement.x)
    {
        speed = speed*-1;
    }
    position.x += speed;
}
void Bot::MoveZ(vect2 zMovement) {

    if (position.z>zMovement.y)
    {
        speed = speed*-1;
    }
    if (position.z<zMovement.x)
    {
        speed = speed*-1;
    }
    position.z += speed;
}

