//
// Created by jlidou on 2022-01-19.
//

#include "Camera.h"
Camera::Camera() {
    position = {10,6,10};
    target = {0,0,0};
}
Camera::Camera(const vect3 &position, const vect3 &target) : position(position), target(target) {}
const vect3 &Camera::getPosition() const {
    return position;
}
void Camera::setPosition(const vect3 &position) {
    Camera::position = position;
}
const vect3 &Camera::getTarget() const {
    return target;
}
void Camera::setTarget(const vect3 &target) {
    Camera::target = target;
}
void Camera::look() {
    gluLookAt(position.x, position.y, position.z, target.x, target.y, target.z, 0, 1, 0);
}

void Camera::cameraFollow(vect3 target, float rotation)
{
    setTarget(target);

    Camera::position.x = 10 * std::sin(M_PI * (270 + rotation)/ 180) + target.x;
    Camera::position.z = 10 * std::cos(M_PI * (270 + rotation)/ 180) + target.z;
}

